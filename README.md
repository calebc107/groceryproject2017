# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Git for the SWE Fall 2017 Project
* Version 1

### How do I get set up? ###

* Summary of set up: 
Right now, this is the root repository for a Visual studio 2017 Solution. We can add other folders for Android Studio/Xarmin components as we go
* Dependencies
Visual Studio 2017 with C#/.NET (v4.5.2) extensions must be installed. 2015 may be fine, but It hasnt been tested


### Contribution guidelines ###

All new features go into the NewFeatures branch.
If youre making another component in visual studio, there should be an option to create a new project within this solution.
Be sure to set references in Visual studio to the StoreCore PROJECT if you need to. This project contains the basic data structure.
Obviously its fine if you need to change something in someone else's code, just be sure to comment out their code instead of 
fully deleting it

### Who do I talk to? ###

Anyone who matters on this project can contact each other in class
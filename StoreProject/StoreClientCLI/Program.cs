﻿using LibStoreCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreClientCLI
{
    class Program
    {
        /// <summary>
        /// The currently loaded store
        /// </summary>
        static Store loadedStore;

        /// <summary>
        /// Basic command line interface to add or update a product
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            string loadedpath;

            try
            {
                //if the user entered anything other than the required 4 arguments, theyre wrong.
                if (args.Length != 4)
                    throw new Exception("Improper usage.");
                loadedpath = args[0];

                //load specefied file
                loadedStore = Store.LoadJson(loadedpath);

                //
                var coordinates = GetProductByName(loadedStore, args[1]);

                //if the product doesnt exist
                if (coordinates[0] < 0)
                {
                    Console.Write(args[1] + " not found. Add it? (Y/n): ");
                    if (Console.ReadLine() != "n")
                        coordinates = AddProduct(args[1]);
                }

                //update price
                loadedStore.sections[coordinates[0]].products[coordinates[1]].priceUSD = double.Parse(args[2]);

                //update availability
                loadedStore.sections[coordinates[0]].products[coordinates[1]].inStock = bool.Parse(args[3]);
                loadedStore.SaveJson(loadedpath);
                Console.Out.WriteLine(args[1] + " updated successfully.");

            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.Message + "\n");

                //command usage
                Console.Out.WriteLine("Usage: \"StoreClientCLI.exe [dbfile.json] [product name] [new price] [true/false]\"" +
                    "\nwhere:" +
                    "\n\tdbfile.json\tfilepath to the store you want to edit's json file" +
                    "\n\tproduct name\tname of the product to edit" +
                    "\n\tnew price\tprice of the selected product" +
                    "\n\ttrue/false\ttrue or false, indicating if the product is in stock\n");
            }
            
        }

        /// <summary>
        /// Return the loacation of the specefied product
        /// </summary>
        /// <param name="store">The store instance to search</param>
        /// <param name="name">The product name to search for (case insensitive)</param>
        /// <returns>returns a 2-dimensional in array, where int[]={[section index][product index]}</returns>
        static int[] GetProductByName(Store store, string name)
        {
            //tries to find the product accosiated with this name
            for (int i = 0; i < store.sections.Count; i++)
            {
                var section = store.sections[i];
                for (int j = 0; j < section.products.Count; j++)
                {
                    var product = section.products[j];
                    if (product.name.ToLower() == name.ToLower())
                    {
                        return new int[] { i, j };
                    }
                }
            }
            return new int[] { -1, -1 };
        }

        /// <summary>
        /// Return the loacation of the specefied section
        /// </summary>
        /// <param name="store">The store instance to search</param>
        /// <param name="name">The section name to search for (case insensitive)</param>
        /// <returns>returns the index of the section in store.sections}</returns>
        static int GetSectionByName(Store store, string name)
        {
            //tries to find the product accosiated with this name
            for (int i = 0; i < store.sections.Count; i++)
            {
                var section = store.sections[i];
                if (section.name == name)
                {
                    return i;
                }
            }
            //else return -1
            return -1;
        }

        /// <summary>
        /// Adds a product of a specefied name. this method asks the user what section to put it in
        /// </summary>
        /// <param name="name">name of the product to add</param>
        /// <returns></returns>
        static int[] AddProduct(string name)
        {
            Console.Write("Enter Section name: (");
            for (int j = 0; j < loadedStore.sections.Count; j++)
                Console.Write(loadedStore.sections[j].name + ", ");
            Console.Write("\b\b): ");
            var secname = Console.ReadLine();
            int i = GetSectionByName(loadedStore, secname);
            loadedStore.sections[i].AddProduct(new Product(name, 0));
            return GetProductByName(loadedStore, name);
        }
    }
}

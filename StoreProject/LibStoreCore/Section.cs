﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibStoreCore
{
    /// <summary>
    /// A section of a store that contains many products
    /// </summary>
    public class Section
    {
        /// <summary>
        /// name of section
        /// </summary>
        public string name;
        /// <summary>
        /// relative x-position of the section, from the bottom right corner of the store, of the section
        /// </summary>
        public double xLoc;

        /// <summary>
        /// relative y-position of the section, from the bottom right corner of the store, of the section
        /// </summary>
        public double yLoc;

        /// <summary>
        /// Products within this section
        /// </summary>
        public List<Product> products;

        /// <summary>
        /// Sections that this section is connected to
        /// </summary>
        public List<Section> sectionconnections;

        //default constructor for json support
        private Section()
        {

        }

        /// <summary>
        /// The standard null section
        /// </summary>
        public static Section nul = new Section("null", 10000, 10000);

        /// <summary>
        /// Creates a new Section
        /// </summary>
        /// <param name="name">Name of section</param>
        /// <param name="xLoc">relative x-position of the section, from the bottom right corner of the store, of the section</param>
        /// <param name="yLoc">relative y-position of the section, from the bottom right corner of the store, of the section</param>
        public Section(string name, double xLoc, double yLoc)
        {
            this.name = name;
            this.xLoc = xLoc;
            this.yLoc = yLoc;
            products = new List<Product>();
            sectionconnections = new List<Section>();
        }

        /// <summary>
        /// add a product to the list of products in that section.
        /// </summary>
        /// <param name="product"></param>
        public void AddProduct(Product product)
        {
            if (!products.Contains(product))
                products.Add(product);
        }

        /// <summary>
        /// Overrides to return the name of the section
        /// </summary>
        /// <returns>The name of the section</returns>
        public override string ToString()
        {
            return name;
        }

        /// <summary>
        /// Connects this section to another section
        /// </summary>
        /// <param name="dest">the section to connect to</param>
        public void ConnectTo(Section dest)
        {
            sectionconnections.Add(dest);
            dest.sectionconnections.Add(this);
        }
    }
}

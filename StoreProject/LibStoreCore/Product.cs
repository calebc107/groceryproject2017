﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibStoreCore
{
    /// <summary>
    /// A product in a store, contained within a section
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Name of the product
        /// </summary>
        public string name;

        /// <summary>
        /// Boolean value indicating whether or not the product is in stock.
        /// this can be updated by stores
        /// </summary>
        public bool inStock;

        /// <summary>
        /// The price of the product, in USD
        /// </summary>
        public double priceUSD;

        //default constructor only here for json support
        private Product()
        {

        }

        /// <summary>
        /// Creates a new product
        /// </summary>
        /// <param name="name">The name of the product</param>
        /// <param name="priceUSD">Price of the product</param>
        public Product(string name, double priceUSD)
        {
            this.name = name;
            this.priceUSD = priceUSD;
            this.inStock = true;
        }

        /// <summary>
        /// Overrides the toString method to return this product's name
        /// </summary>
        /// <returns>the name of the product</returns>
        public override string ToString()
        {
            return name;
        }
    }
}

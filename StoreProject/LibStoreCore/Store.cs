﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Drawing.Imaging;

namespace LibStoreCore
{
    /// <summary>
    /// A store. This is the base unit for each database file
    /// </summary>
    public class Store
    {
        /// <summary>
        /// Name of the store
        /// </summary>
        public string storeName;

        /// <summary>
        /// Hours the store is open (ex: "9am-9pm")
        /// </summary>
        public string storeHours;

        /// <summary>
        /// the address of the store
        /// </summary>
        public string address;

        /// <summary>
        /// A list of sections the store has
        /// </summary>
        public List<Section> sections;

        /// <summary>
        /// The image to be used as the background, represented as base64
        /// </summary>
        public string mapbase64;

        /// <summary>
        /// the default json serializer settings. Enforces looped references, etc.
        /// </summary>
        public static JsonSerializerSettings settings = new JsonSerializerSettings
        {
            PreserveReferencesHandling = PreserveReferencesHandling.All,
            Formatting = Formatting.Indented,
            ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,

        };

        //for json support
        private Store()
        {

        }

        /// <summary>
        /// Creates a new instance of a store
        /// </summary>
        /// <param name="storeName">Name of the store</param>
        /// <param name="storeHours">Hours the store is open</param>
        /// <param name="address">Address of the store</param>
        /// <param name="mapbase64">Map of store, represented as an image in Base64 string format</param>
        public Store(string storeName, string storeHours, string address, string mapbase64)
        {
            this.storeName = storeName;
            this.address = address;
            this.storeHours = storeHours;
            sections = new List<Section>();
            this.mapbase64 = mapbase64;
        }

        /// <summary>
        /// Adds a section to the list of sections the store has
        /// </summary>
        /// <param name="section">The section to add</param>
        public void AddSection(Section section)
        {
            // verify new section doesnt already exist, otherwise create new section name
            if (!sections.Contains(section))
            {
                sections.Add(section);
            }
        }

        /// <summary>
        /// Save the current store, and all its properties, to a json file
        /// </summary>
        /// <param name="path">the relitave path of the json file</param>
        public void SaveJson(string path)
        {
            string output = JsonConvert.SerializeObject(this, settings);
            File.WriteAllText(path, output);
        }

        /// <summary>
        /// Converts an image into its Base64 representation
        /// </summary>
        /// <param name="image">The image to convert</param>
        /// <returns>A Base64 string of the image</returns>
        public static string Image2Base64(Image image)
        {
            MemoryStream ms = new MemoryStream();
            image.Save(ms, ImageFormat.Png);
            return Convert.ToBase64String(ms.ToArray());
        }

        /// <summary>
        /// Converst a Base64 string into an image
        /// </summary>
        /// <param name="base64">the Base64 string to convert into an image</param>
        /// <returns></returns>
        public static Image Base642Image(string base64)
        {
            MemoryStream ms = new MemoryStream(Convert.FromBase64String(base64));
            Image image = Image.FromStream(ms);
            return image;
        }

        /// <summary>
        /// Load the current store, and all its properties, from a json file
        /// </summary>
        /// <param name="path">the relitave path of the json file</param>
        public static Store LoadJson(string path)
        {
            string input = File.ReadAllText(path);
            return (Store)JsonConvert.DeserializeObject(input, typeof(Store), settings);
        }
        /// <summary>
        /// Finds the most optimal path through the store, given the products needed
        /// </summary>
        /// <param name="shoppinglist">string[] representing the products to find</param>
        /// <returns>A list of sections starting at the enterance that represents the best path through the store</returns>
        public List<Section> FindPathByProducts(string[] shoppinglist)
        {
            var result = ProductStringsToSections(shoppinglist);

            //sort
            result = FindPathBySection(result);
            return result;
        }

        /// <summary>
        /// Finds the most potimal path through the store, given the sections needed
        /// </summary>
        /// <param name="sections"></param>
        /// <returns>A list of sections starting at the enterance that represents the best path through the store</returns>
        private List<Section> FindPathBySection(List<Section> sections)
        {
            var result = new List<Section>();
            var start = GetEnteranceNode(); //should always be enterance node
            while (sections.Count > 0) //will always have enterance node
            {
                var bestdist = double.MaxValue;
                var bestpath = new List<Section>();
                for (int i = 0; i < sections.Count; i++)
                {
                    var currentdest = sections[i];
                    List<Section> currentpath = GetPath(start, currentdest, new List<Section>());
                    var currentdist = GetDistance(currentpath);
                    if (currentdist < bestdist)
                    {
                        bestpath = currentpath;
                    }
                }
                result.AddRange(bestpath);
                start = result[result.Count - 1];
                foreach (var section in bestpath)
                {
                    sections.Remove(section);
                }
            }
            return result;
        }

        /// <summary>
        /// Looks for the section in the current store named "Entrance"
        /// </summary>
        /// <returns>This sotre's entrance node</returns>
        private Section GetEnteranceNode()
        {
            foreach (Section section in sections)
            {
                if (section.name.ToLower().Equals("entrance"))
                    return section;
            }
            throw new Exception("entrance not found");
        }

        /// <summary>
        /// Looks for a path starting at start and ending at dest.
        /// </summary>
        /// <param name="start">The section to start from</param>
        /// <param name="dest">The section to try to find a path to</param>
        /// <param name="previous">used for rrecursion, a list of sections that have already been passed in this path</param>
        /// <returns>A list of sections in order from start to dest, with all sections to be passed through to get there</returns>
        private List<Section> GetPath(Section start, Section dest, List<Section> previous)
        {
            //Console.WriteLine(start + "-" + dest);
            //check to see if start is directly connected to dest
            if (start.sectionconnections.Contains(dest))
                return new List<Section>() { start, dest };

            //otherwise, check all subconnections and return the path with the best value
            List<Section> bestpath = new List<Section>() { Section.nul };
            var bestdistance = double.MaxValue;
            var closestsection = start.sectionconnections[0];
            for (int i = 0; i < start.sectionconnections.Count; i++)
            {
                var newstart = start.sectionconnections[i];
                if (!previous.Contains(newstart))
                {
                    var newprevious = previous;
                    newprevious.Add(start);

                    //recureively try to find a path from the next layer of nodes
                    var newpath = new List<Section>() { Section.nul };
                    if (GetDistance(newstart, dest) < GetDistance(start, dest)+25) //if this node is closer than the best
                    {
                        newpath = GetPath(newstart, dest, newprevious);
                    }
                    var newdistance = GetDistance(newpath);
                    if (newdistance < bestdistance)
                    {
                        bestpath = newpath;
                        bestdistance = newdistance;
                    }
                }
            }
            var finalpath = new List<Section> { start };
            finalpath.AddRange(bestpath);
            return finalpath;
        }

        private double GetDistance(Section start, Section dest)
        {
            var x1 = start.xLoc;
            var y1 = start.yLoc;
            var x2 = dest.xLoc;
            var y2 = dest.yLoc;
            return Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
        }

        /// <summary>
        /// Searches for all products or sections, and provides a list of their respective sections
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private List<Section> ProductStringsToSections(string[] list)
        {
            var result = new List<Section>();

            //for every item requested, check every section and item for a match
            foreach (string item in list)
                foreach (Section sec in sections)
                {
                    if (!result.Contains(sec) && item.ToLower() == sec.name.ToLower())
                        result.Add(sec);
                    foreach (Product prod in sec.products)
                       
                        if (!result.Contains(sec) && (prod.name.ToLower() == item.ToLower())) //if the requested name equals a specific product or section, add that section
                            result.Add(sec);
                }
            return result;
        }


        /// <summary>
        /// Findthe distance between sections
        /// </summary>
        /// <param name="sections">The path of sections to get the distance of</param>
        /// <returns>The distance of the specefied path</returns>
        double GetDistance(List<Section> sections)
        {
            double result = 0;
            if (sections.Contains(Section.nul))
            {
                return double.MaxValue;
            }
            for (int i = 0; i < sections.Count - 1; i++)
            {
                var x1 = sections[i].xLoc;
                var y1 = sections[i].yLoc;
                var x2 = sections[i + 1].xLoc;
                var y2 = sections[i + 1].yLoc;
                var distance = Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
                result += distance;
            }
            return result;
        }

        /// <summary>
        /// creates an image that shows the sections in the store
        /// </summary>
        /// <returns>The image of the store background, with the sections overlayed on top of it</returns>
        public Image DrawSections()
        {
            //draw stuff
            Image bgimage = Base642Image(mapbase64);
            var g = Graphics.FromImage(bgimage);
            var p = new Pen(Color.FromKnownColor(KnownColor.Blue), (float)2.0);
            foreach (Section section in this.sections)
            {
                var x = (float)section.xLoc;
                var y = (float)section.yLoc;
                g.FillEllipse(p.Brush, x - 5, y - 5, 10, 10);
                foreach (Section destsection in section.sectionconnections)
                {
                    var x1 = (float)section.xLoc;
                    var y1 = (float)section.yLoc;
                    var x2 = (float)destsection.xLoc;
                    var y2 = (float)destsection.yLoc;
                    g.DrawLine(p, x1, y1, x2, y2);
                }
            }
            return bgimage;
        }

        /// <summary>
        /// creates an image that shows the sections in the store witha path drawn over the specefied sections
        /// </summary>
        /// <param name="path">the path through the store to draw</param>
        /// <returns>The image of the store background, with the sections and specefied path overlayed on top of it</returns>
        public Image DrawSections(List<Section> path)
        {
            Image bgimage = DrawSections();
            var g = Graphics.FromImage(bgimage);
            var p = new Pen(Color.FromKnownColor(KnownColor.Orange), (float)2.5);
            for (int i = 0; i < path.Count; i++)
            {
                var section = path[i];
                var x = (float)section.xLoc;
                var y = (float)section.yLoc;
                if (path.Contains(section))
                {
                    //todo: only highlight nodes relevant to path
                }
                g.FillEllipse(p.Brush, x - 5, y - 5, 10, 10);
                for (int j = 0; j < section.sectionconnections.Count; j++)
                {
                    var destsection = section.sectionconnections[j];
                    if (i < path.Count - 1 && destsection == path[i + 1])
                    {
                        var x1 = (float)section.xLoc;
                        var y1 = (float)section.yLoc;
                        var x2 = (float)destsection.xLoc;
                        var y2 = (float)destsection.yLoc;
                        g.DrawLine(p, x1, y1, x2, y2);
                    }
                }
            }
            return bgimage;
        }
    }
}

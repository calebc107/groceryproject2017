﻿using LibStoreCore;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserClientCLI
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //load the specefied json file
                Store store = Store.LoadJson(args[0]);

                //list all products
                if (args[1] == "list")
                {
                    foreach (var section in store.sections)
                    {
                        Console.Write("\n" + section.name + ":");
                        foreach (var product in section.products)
                        {
                            Console.Write(' ' + product.name);
                        }
                    }
                }
                else
                {
                    //if we're not listing everything, treat everyother argument as a product to find
                    List<string> shoppingList = new List<string>(args);
                    shoppingList.RemoveAt(0); //remove the json file argument
                    List<Section> path = store.FindPathByProducts(shoppingList.ToArray());
                    store.DrawSections(path).Save("path.png", ImageFormat.Png);

                    //print out the resulting path that was found
                    Console.Write("Best path: ");
                    for (int i = 0; i < path.Count(); i++)
                    {
                        Console.Write(path[i].name + ", ");
                    }
                    Console.WriteLine("\b\b\nsaved map as path.png");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\nERROR: " + e.Message + "\n");
                Console.WriteLine("Draws a map of the store with a path to the specefied products, and saves it as \"path.png\"\n\n" +
                    "Usage: \"UserClientCLI.exe [StoreFile.json] {list} {products to find}\"\n\n" +
                    "\twhere {products to find} is a space-seperrated list of products\n" +
                    "\tor {list} is the command that lists all sections and products in the store");
            }
        }
    }
}
﻿using System;
using System.Drawing;
using System.Windows.Forms;
using LibStoreCore;
using StoreClient;

namespace MainEditor
{
    public partial class storeDesigner : Form
    {
        /// <summary>
        /// The current store instance that is being edited
        /// </summary>
        public static Store loadedStore;

        /// <summary>
        /// the image size to scale the background image to
        /// </summary>
        Size imageSize;

        /// <summary>
        ///The currently loaded store's json filepath
        /// </summary>
        string loadedpath = "\\ ";

        /// <summary>
        /// creates a new instance of the store designer form
        /// </summary>
        public storeDesigner()
        {
            InitializeComponent();
            bigOlBox.MouseMove += delegate { label4.Text = (MousePosition.X - Location.X - 19).ToString() + ", " + (MousePosition.Y - Location.Y - 115).ToString()+", "+FindSectionAt(MousePosition.X - Location.X - 19, MousePosition.Y - Location.Y - 115); };
            imageSize = bigOlBox.Size;
            Image initImage = new Bitmap(imageSize.Width, Size.Height);
            Graphics.FromImage(initImage).FillRectangle(new Pen(Color.White).Brush, 0, 0, Size.Width, Size.Height);
            loadedStore = new Store("", "", "", Store.Image2Base64(initImage));
        }

        /// <summary>
        /// Updates the UI to reflect the currently loaded store
        /// </summary>
        public void UpdateUI()
        {
            nameTextBox.Text = loadedStore.storeName;
            addressBox.Text = loadedStore.address;
            hoursBox.Text = loadedStore.storeHours;
            bigOlBox.BackgroundImage = loadedStore.DrawSections();
            filename.Text = loadedpath.Substring(loadedpath.LastIndexOf('\\') + 1);
            saveButton_Click(null, null);

        }

        /// <summary>
        /// Load a store from a JSON file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadButton_Click(object sender, EventArgs e)
        {
            var openDialog = new OpenFileDialog();
            openDialog.Filter = "JSON Files|*.json";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                loadedStore = Store.LoadJson(openDialog.FileName);
                loadedpath = openDialog.FileName;
            }
            UpdateUI();
        }

        /// <summary>
        /// Initiates a window to create or edit a section
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bigOlBox_Click(object sender, EventArgs e)
        {
            try
            {
                double posx = MousePosition.X - Location.X - 19;
                double posy = MousePosition.Y - Location.Y - 115;
                Section currentsection = FindSectionAt(posx, posy);
                var form = new InsertSection(currentsection);
                if (form.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        loadedStore.sections.Remove(currentsection);
                    }
                    catch { }
                    loadedStore.sections.Add(form.resultSection);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            UpdateUI();
        }

        /// <summary>
        /// Finds a section at the given coordinates, or creates a new section at the coordinates
        /// </summary>
        /// <param name="x">x-coordinate to find section at</param>
        /// <param name="y">y-coordinate to find section at</param>
        /// <returns>The section at the specefied coordinates</returns>
        Section FindSectionAt(double x, double y)
        {
            foreach (var section in loadedStore.sections)
            {
                if (Math.Abs(section.xLoc - x) <= 5 && Math.Abs(section.yLoc - y) <= 5)
                {
                    return section;
                }
            }
            return new Section("", x, y);
        }

        /// <summary>
        /// Loads and sets the background image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BGChange(object sender, EventArgs e)
        {
            var openDialog = new OpenFileDialog();
            openDialog.Filter = "Image|*.png;*.bmp;*.jpg;*.jpeg";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                var image = Image.FromFile(openDialog.FileName);
                var newimage = new Bitmap(image, imageSize);
                loadedStore.mapbase64 = Store.Image2Base64(newimage);
            }
            UpdateUI();
        }

        /// <summary>
        /// Saves the current instance of the store being edited
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveButton_Click(object sender, EventArgs e)
        {
            loadedStore.storeName = nameTextBox.Text;
            loadedStore.address = addressBox.Text;
            loadedStore.storeHours = hoursBox.Text;
            if (loadedpath != "\\ ")
                loadedStore.SaveJson(loadedpath);
            else
            {
                var saveDialog = new SaveFileDialog();
                saveDialog.Filter = "JSON Files|*.json";
                if (saveDialog.ShowDialog() == DialogResult.OK)
                {
                    loadedStore.SaveJson(saveDialog.FileName);
                    loadedpath = saveDialog.FileName;
                }
            }
        }
    }
}

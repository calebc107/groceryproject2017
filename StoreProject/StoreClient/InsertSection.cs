﻿using LibStoreCore;
using MainEditor;
using System;
using System.Linq;
using System.Windows.Forms;

namespace StoreClient
{
    public partial class InsertSection : Form
    {
        /// <summary>
        /// the final verdion of the edited section, is assigned after the OK button is pressed.
        /// </summary>
        public Section resultSection;

        /// <summary>
        /// The loaded section to be used for local editing
        /// </summary>
        Section loadedSection;

        /// <summary>
        /// Loads a new instance of the InsertSection form.
        /// </summary>
        /// <param name="section">The section to initially load into the form</param>
        public InsertSection(Section section)
        {
            InitializeComponent();
            NameBox.Leave += NameBox_Leave;

            loadedSection = section;
            UpdateUI();
        }

        /// <summary>
        /// Automatically saves the new section name when it is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NameBox_Leave(object sender, EventArgs e)
        {
            if (NameBox.Text == "")
            {
                MessageBox.Show("Name cannot be blank");
            }
            loadedSection.name = NameBox.Text;
        }

        /// <summary>
        /// Updates the UI to reflect the currently loaded section
        /// </summary>
        void UpdateUI()
        {
            NameBox.Text = loadedSection.name;
            productListBox.Items.Clear();
            connectionListBox.Items.Clear();
            UpdateProductList();
            UpdateConnectionsList();
        }

        /// <summary>
        /// Reloads the UI's list of products tr reflect the loaded loaded section's products
        /// </summary>
        void UpdateProductList()
        {
            productListBox.Items.Clear();
            for (int i = 0; i < loadedSection.products.Count; i++)
            {
                productListBox.Items.Add(loadedSection.products[i]);
            }
        }
        /// <summary>
        /// Reloads the UI's list of products tr reflect the loaded loaded section's connections
        /// </summary>
        void UpdateConnectionsList()
        {
            connectionListBox.Items.Clear();
            for (int i = 0; i < loadedSection.sectionconnections.Count; i++)
            {
                connectionListBox.Items.Add(loadedSection.sectionconnections[i].name);
            }
        }

        /// <summary>
        /// Closes the form and sets the productResult to the loaded, edited, section
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OKButton_Click(object sender, EventArgs e)
        {
            if (NameBox.Text == "")
            {
                MessageBox.Show("Name cannot be blank");
            }
            else
            {
                loadedSection.name = NameBox.Text;
                DialogResult = DialogResult.OK;
                resultSection = loadedSection;
                Close();
            }

        }

        /// <summary>
        /// Closes the form without saving the edited section
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Creates a new instance of AddProduct form with a new blank product to be edited/added
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddProdButton_Click(object sender, EventArgs e)
        {
            try
            {
                var productform = new InsertProduct(new Product("", 0.00));
                if (productform.ShowDialog() == DialogResult.OK)
                {
                    loadedSection.AddProduct(productform.productResult);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            UpdateUI();
        }

        /// <summary>
        /// Creates a new instance of AddProduct form with the selected product to be edited
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditProdButton_Click(object sender, EventArgs e)
        {
            try
            {
                int i = productListBox.SelectedIndex;
                if (i < 0)
                    return;
                var editForm = new InsertProduct(loadedSection.products[i]);
                if (editForm.ShowDialog() == DialogResult.OK)
                {
                    var newProd = editForm.productResult;
                    loadedSection.products.RemoveAt(i);
                    loadedSection.products.Insert(i, newProd);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            UpdateUI();
        }

        /// <summary>
        /// Deletes the currently selected product
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteProdButton_Click(object sender, EventArgs e)
        {
            try
            {
                int i = productListBox.SelectedIndex;
                loadedSection.products.RemoveAt(i);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            UpdateUI();
        }

        /// <summary>
        /// Creates a new instance of AddConnection form with a new blank connection to be added
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddConButton_Click(object sender, EventArgs e)
        {
            try
            {
                var newConnForm = new AddConnection(loadedSection);
                newConnForm.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            UpdateUI();
        }

        /// <summary>
        /// Deletes the currently selected connection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DelConButton_Click(object sender, EventArgs e)
        {
            int i = connectionListBox.SelectedIndex;
            Section dest = loadedSection.sectionconnections.ElementAt(i);
            dest.sectionconnections.Remove(loadedSection);
            loadedSection.sectionconnections.Remove(dest);
            UpdateUI();
        }
        /// <summary>
        /// deletes the currently loaded section, and all connections to it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteButton_Click(object sender, EventArgs e)
        {
            foreach (Section section in loadedSection.sectionconnections)
            {
                section.sectionconnections.Remove(loadedSection);
            }
            storeDesigner.loadedStore.sections.Remove(loadedSection);
            Close();
        }
    }
}

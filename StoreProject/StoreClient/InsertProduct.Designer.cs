﻿namespace StoreClient
{
    partial class InsertProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.NameBox = new System.Windows.Forms.TextBox();
            this.InStockCheckbox = new System.Windows.Forms.CheckBox();
            this.PricePicker = new System.Windows.Forms.NumericUpDown();
            this.OKButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.PricePicker)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Price:";
            // 
            // NameBox
            // 
            this.NameBox.Location = new System.Drawing.Point(53, 12);
            this.NameBox.Name = "NameBox";
            this.NameBox.Size = new System.Drawing.Size(100, 20);
            this.NameBox.TabIndex = 3;
            // 
            // InStockCheckbox
            // 
            this.InStockCheckbox.AutoSize = true;
            this.InStockCheckbox.Checked = true;
            this.InStockCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.InStockCheckbox.Location = new System.Drawing.Point(52, 65);
            this.InStockCheckbox.Name = "InStockCheckbox";
            this.InStockCheckbox.Size = new System.Drawing.Size(66, 17);
            this.InStockCheckbox.TabIndex = 4;
            this.InStockCheckbox.Text = "In Stock";
            this.InStockCheckbox.UseVisualStyleBackColor = true;
            // 
            // PricePicker
            // 
            this.PricePicker.DecimalPlaces = 2;
            this.PricePicker.Location = new System.Drawing.Point(52, 39);
            this.PricePicker.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.PricePicker.Name = "PricePicker";
            this.PricePicker.Size = new System.Drawing.Size(101, 20);
            this.PricePicker.TabIndex = 5;
            this.PricePicker.ThousandsSeparator = true;
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(12, 88);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 6;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(93, 88);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 7;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // InsertProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(181, 120);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.PricePicker);
            this.Controls.Add(this.InStockCheckbox);
            this.Controls.Add(this.NameBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InsertProduct";
            this.ShowIcon = false;
            this.Text = "InsertProduct";
            ((System.ComponentModel.ISupportInitialize)(this.PricePicker)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox NameBox;
        private System.Windows.Forms.CheckBox InStockCheckbox;
        private System.Windows.Forms.NumericUpDown PricePicker;
        private System.Windows.Forms.Button OKButton;
        private new System.Windows.Forms.Button CancelButton;
    }
}
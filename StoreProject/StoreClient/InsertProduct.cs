﻿using LibStoreCore;
using System;
using System.Windows.Forms;

namespace StoreClient
{
    public partial class InsertProduct : Form
    {
        /// <summary>
        /// the final verdion of the edited product, is assigned after the OK button is pressed.
        /// </summary>
        public Product productResult;

        /// <summary>
        /// The loaded product to be used for local editing
        /// </summary>
        Product loadedProduct;

        /// <summary>
        /// Loads a new instance of the AddProduct form.
        /// </summary>
        /// <param name="product">The product to initially load into the form</param>
        public InsertProduct(Product product)
        {
            InitializeComponent();
            loadedProduct = product;
            UpdateUI();
        }

        /// <summary>
        /// Updates the UI to reflect the currently loaded product
        /// </summary>
        void UpdateUI()
        {
            NameBox.Text = loadedProduct.name;
            PricePicker.Value = (decimal)loadedProduct.priceUSD;
            InStockCheckbox.Checked = loadedProduct.inStock;
        }

        /// <summary>
        /// Closes the form and sets the productResult to the loaded, edited, product
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OKButton_Click(object sender, EventArgs e)
        {
            loadedProduct.name = NameBox.Text;
            loadedProduct.priceUSD = (double)PricePicker.Value;
            loadedProduct.inStock = InStockCheckbox.Checked;
            DialogResult = DialogResult.OK;
            productResult = loadedProduct;
            Close();
        }

        /// <summary>
        /// Closes the form without saving the edited product
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}

﻿namespace StoreClient
{
    partial class InsertSection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.NameBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.DelConButton = new System.Windows.Forms.Button();
            this.OKButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.connectionListBox = new System.Windows.Forms.ListBox();
            this.productListBox = new System.Windows.Forms.ListBox();
            this.AddConButton = new System.Windows.Forms.Button();
            this.AddProdButton = new System.Windows.Forms.Button();
            this.DeleteProdButton = new System.Windows.Forms.Button();
            this.EditProdButton = new System.Windows.Forms.Button();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Section name:";
            // 
            // NameBox
            // 
            this.NameBox.Location = new System.Drawing.Point(94, 10);
            this.NameBox.Name = "NameBox";
            this.NameBox.Size = new System.Drawing.Size(125, 20);
            this.NameBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Products:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Connections:";
            // 
            // DelConButton
            // 
            this.DelConButton.Location = new System.Drawing.Point(225, 153);
            this.DelConButton.Name = "DelConButton";
            this.DelConButton.Size = new System.Drawing.Size(104, 23);
            this.DelConButton.TabIndex = 11;
            this.DelConButton.Text = "Delete Connection";
            this.DelConButton.UseVisualStyleBackColor = true;
            this.DelConButton.Click += new System.EventHandler(this.DelConButton_Click);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(79, 212);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 12;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(161, 212);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 13;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // connectionListBox
            // 
            this.connectionListBox.FormattingEnabled = true;
            this.connectionListBox.Location = new System.Drawing.Point(94, 124);
            this.connectionListBox.Name = "connectionListBox";
            this.connectionListBox.Size = new System.Drawing.Size(125, 82);
            this.connectionListBox.TabIndex = 14;
            // 
            // productListBox
            // 
            this.productListBox.FormattingEnabled = true;
            this.productListBox.Location = new System.Drawing.Point(94, 36);
            this.productListBox.Name = "productListBox";
            this.productListBox.Size = new System.Drawing.Size(125, 82);
            this.productListBox.TabIndex = 15;
            // 
            // AddConButton
            // 
            this.AddConButton.Location = new System.Drawing.Point(225, 124);
            this.AddConButton.Name = "AddConButton";
            this.AddConButton.Size = new System.Drawing.Size(104, 23);
            this.AddConButton.TabIndex = 16;
            this.AddConButton.Text = "Add Connection";
            this.AddConButton.UseVisualStyleBackColor = true;
            this.AddConButton.Click += new System.EventHandler(this.AddConButton_Click);
            // 
            // AddProdButton
            // 
            this.AddProdButton.Location = new System.Drawing.Point(225, 36);
            this.AddProdButton.Name = "AddProdButton";
            this.AddProdButton.Size = new System.Drawing.Size(104, 23);
            this.AddProdButton.TabIndex = 17;
            this.AddProdButton.Text = "Add Product";
            this.AddProdButton.UseVisualStyleBackColor = true;
            this.AddProdButton.Click += new System.EventHandler(this.AddProdButton_Click);
            // 
            // DeleteProdButton
            // 
            this.DeleteProdButton.Location = new System.Drawing.Point(225, 95);
            this.DeleteProdButton.Name = "DeleteProdButton";
            this.DeleteProdButton.Size = new System.Drawing.Size(103, 23);
            this.DeleteProdButton.TabIndex = 18;
            this.DeleteProdButton.Text = "Delete Product";
            this.DeleteProdButton.UseVisualStyleBackColor = true;
            this.DeleteProdButton.Click += new System.EventHandler(this.DeleteProdButton_Click);
            // 
            // EditProdButton
            // 
            this.EditProdButton.Location = new System.Drawing.Point(226, 66);
            this.EditProdButton.Name = "EditProdButton";
            this.EditProdButton.Size = new System.Drawing.Size(102, 23);
            this.EditProdButton.TabIndex = 19;
            this.EditProdButton.Text = "Edit Product";
            this.EditProdButton.UseVisualStyleBackColor = true;
            this.EditProdButton.Click += new System.EventHandler(this.EditProdButton_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.Location = new System.Drawing.Point(243, 212);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(85, 23);
            this.DeleteButton.TabIndex = 20;
            this.DeleteButton.Text = "Delete Section";
            this.DeleteButton.UseVisualStyleBackColor = true;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // InsertSection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(345, 244);
            this.Controls.Add(this.DeleteButton);
            this.Controls.Add(this.EditProdButton);
            this.Controls.Add(this.DeleteProdButton);
            this.Controls.Add(this.AddProdButton);
            this.Controls.Add(this.AddConButton);
            this.Controls.Add(this.productListBox);
            this.Controls.Add(this.connectionListBox);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.DelConButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.NameBox);
            this.Controls.Add(this.label1);
            this.Name = "InsertSection";
            this.Text = "InsertSection";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox NameBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button DelConButton;
        private System.Windows.Forms.Button OKButton;
        private new System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.ListBox connectionListBox;
        private System.Windows.Forms.ListBox productListBox;
        private System.Windows.Forms.Button AddConButton;
        private System.Windows.Forms.Button AddProdButton;
        private System.Windows.Forms.Button DeleteProdButton;
        private System.Windows.Forms.Button EditProdButton;
        private System.Windows.Forms.Button DeleteButton;
    }
}
﻿using LibStoreCore;
using MainEditor;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace StoreClient
{
    public partial class AddConnection : Form
    {
        /// <summary>
        /// The section to add the new connection to
        /// </summary>
        Section source;

        /// <summary>
        /// the sections of the currently loaded section
        /// </summary>
        List<Section> sections;

        /// <summary>
        /// Creates a new instance of the AddConnection form
        /// </summary>
        /// <param name="source">The section to add the new connection to</param>
        public AddConnection(Section source)
        {
            InitializeComponent();
            sections= storeDesigner.loadedStore.sections;
            this.source = source;
            UpdateUI();
        }

        /// <summary>
        /// Proxy to update UI with all of the possible sections in the dropdown
        /// </summary>
        void UpdateUI()
        {
            getConnections();
        }

        /// <summary>
        /// Loads all of the current sections into this form's dropdown menu
        /// </summary>
        private void getConnections()
        {
            ConnectionsComboBox.Items.Clear();
            
            for (int i = 0; i < sections.Count; i++)
            {
                ConnectionsComboBox.Items.Add(sections[i].name);
            }
        }

        /// <summary>
        /// Connects the currently loaded section to the one selected in the dropdown menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Connect_Click(object sender, EventArgs e)
        {
            var destination = sections[ConnectionsComboBox.SelectedIndex];
            source.ConnectTo(destination);
            //source.sectionconnections.Add(destination);
            //destination.sectionconnections.Add(source);
            DialogResult = DialogResult.OK;
        }
    }
}

﻿namespace StoreClient
{
    partial class AddConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ConnectionsComboBox = new System.Windows.Forms.ComboBox();
            this.Connect = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ConnectionsComboBox
            // 
            this.ConnectionsComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.ConnectionsComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ConnectionsComboBox.FormattingEnabled = true;
            this.ConnectionsComboBox.Location = new System.Drawing.Point(13, 13);
            this.ConnectionsComboBox.Name = "ConnectionsComboBox";
            this.ConnectionsComboBox.Size = new System.Drawing.Size(147, 21);
            this.ConnectionsComboBox.TabIndex = 0;
            // 
            // Connect
            // 
            this.Connect.Location = new System.Drawing.Point(167, 13);
            this.Connect.Name = "Connect";
            this.Connect.Size = new System.Drawing.Size(75, 21);
            this.Connect.TabIndex = 1;
            this.Connect.Text = "Connect";
            this.Connect.UseVisualStyleBackColor = true;
            this.Connect.Click += new System.EventHandler(this.Connect_Click);
            // 
            // AddConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(248, 43);
            this.Controls.Add(this.Connect);
            this.Controls.Add(this.ConnectionsComboBox);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddConnection";
            this.ShowIcon = false;
            this.Text = "AddConnection";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox ConnectionsComboBox;
        private System.Windows.Forms.Button Connect;
    }
}
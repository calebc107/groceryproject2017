﻿namespace FrontEndUI
{
    partial class ListCreator
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListCreator));
            this.ItemSearchBox = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.AddItemToList = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.CreateNewList = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.DeleteItem = new System.Windows.Forms.Button();
            this.ListBox = new System.Windows.Forms.ListBox();
            this.ListName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // ItemSearchBox
            // 
            this.ItemSearchBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ItemSearchBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ItemSearchBox.Location = new System.Drawing.Point(37, 317);
            this.ItemSearchBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ItemSearchBox.Name = "ItemSearchBox";
            this.ItemSearchBox.Size = new System.Drawing.Size(182, 22);
            this.ItemSearchBox.TabIndex = 0;
            this.ItemSearchBox.TextChanged += new System.EventHandler(this.ItemSearchBox_TextChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.AddItemToList);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.ItemSearchBox);
            this.panel1.Location = new System.Drawing.Point(27, 57);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(350, 558);
            this.panel1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(32, 231);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(294, 64);
            this.label1.TabIndex = 3;
            this.label1.Text = "Enter the item you are looking for and add \r\nthem to your new list. There is a li" +
    "mit of 4 \r\nlists that you can create before editing and \r\nremoving lists is requ" +
    "ired.";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(35, 347);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(277, 185);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // AddItemToList
            // 
            this.AddItemToList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddItemToList.ForeColor = System.Drawing.Color.Black;
            this.AddItemToList.Location = new System.Drawing.Point(229, 299);
            this.AddItemToList.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.AddItemToList.Name = "AddItemToList";
            this.AddItemToList.Size = new System.Drawing.Size(83, 40);
            this.AddItemToList.TabIndex = 2;
            this.AddItemToList.Text = "Add Item";
            this.AddItemToList.UseVisualStyleBackColor = true;
            this.AddItemToList.Click += new System.EventHandler(this.AddItemToList_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(35, 33);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(275, 183);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // CreateNewList
            // 
            this.CreateNewList.BackColor = System.Drawing.Color.Silver;
            this.CreateNewList.FlatAppearance.BorderSize = 0;
            this.CreateNewList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateNewList.Location = new System.Drawing.Point(661, 585);
            this.CreateNewList.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CreateNewList.Name = "CreateNewList";
            this.CreateNewList.Size = new System.Drawing.Size(130, 30);
            this.CreateNewList.TabIndex = 3;
            this.CreateNewList.Text = "Create List";
            this.CreateNewList.UseVisualStyleBackColor = false;
            this.CreateNewList.Click += new System.EventHandler(this.CreateNewList_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(236, 32);
            this.label2.TabIndex = 4;
            this.label2.Text = "Add a name to your new list!\r\nEnter your desired list name below.\r\n";
            // 
            // DeleteItem
            // 
            this.DeleteItem.BackColor = System.Drawing.Color.Silver;
            this.DeleteItem.FlatAppearance.BorderSize = 0;
            this.DeleteItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteItem.Location = new System.Drawing.Point(803, 585);
            this.DeleteItem.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DeleteItem.Name = "DeleteItem";
            this.DeleteItem.Size = new System.Drawing.Size(97, 30);
            this.DeleteItem.TabIndex = 5;
            this.DeleteItem.Text = "Delete Item";
            this.DeleteItem.UseVisualStyleBackColor = false;
            this.DeleteItem.Click += new System.EventHandler(this.DeleteItem_Click);
            // 
            // ListBox
            // 
            this.ListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListBox.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListBox.FormattingEnabled = true;
            this.ListBox.ItemHeight = 25;
            this.ListBox.Location = new System.Drawing.Point(667, 138);
            this.ListBox.Name = "ListBox";
            this.ListBox.Size = new System.Drawing.Size(233, 427);
            this.ListBox.TabIndex = 6;
            this.ListBox.SelectedIndexChanged += new System.EventHandler(this.ListBox_SelectedIndexChanged);
            // 
            // ListName
            // 
            this.ListName.Location = new System.Drawing.Point(8, 59);
            this.ListName.Name = "ListName";
            this.ListName.Size = new System.Drawing.Size(214, 22);
            this.ListName.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(658, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(274, 64);
            this.label3.TabIndex = 8;
            this.label3.Text = "The items that you add to the list will \r\ngenerate in the section below, If you \r" +
    "\nadd an item by accident you can select \r\nthe item in the list and remove it.";

            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(396, 57);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(239, 232);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 9;
            this.pictureBox3.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(181)))), ((int)(((byte)(193)))));
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.ListName);
            this.panel2.Location = new System.Drawing.Point(396, 295);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(239, 100);
            this.panel2.TabIndex = 10;

            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(396, 404);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(239, 211);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 11;
            this.pictureBox4.TabStop = false;

            // 
            // ListCreator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ListBox);
            this.Controls.Add(this.DeleteItem);
            this.Controls.Add(this.CreateNewList);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ListCreator";
            this.Size = new System.Drawing.Size(957, 674);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ItemSearchBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button AddItemToList;
        private System.Windows.Forms.Button CreateNewList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button DeleteItem;
        private System.Windows.Forms.ListBox ListBox;
        private System.Windows.Forms.TextBox ListName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox4;
    }
}

﻿using FrontEndUI;
namespace FrontEndUI
{
    partial class MainPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainPage));
            this.SideSelection = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Lists = new System.Windows.Forms.Button();
            this.Map = new System.Windows.Forms.Button();
            this.Home = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.Contact = new System.Windows.Forms.Button();
            this.Settings = new System.Windows.Forms.Button();
            this.contactPage1 = new FrontEndUI.ContactPage();
            this.homePage1 = new FrontEndUI.ListCreator();
            this.mapPage1 = new FrontEndUI.MapPage();
            this.savedLists1 = new FrontEndUI.SavedLists();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SideSelection
            // 
            this.SideSelection.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(207)))), ((int)(((byte)(123)))));
            this.SideSelection.Location = new System.Drawing.Point(4, 209);
            this.SideSelection.Name = "SideSelection";
            this.SideSelection.Size = new System.Drawing.Size(22, 70);
            this.SideSelection.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(164, 88);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // Lists
            // 
            this.Lists.FlatAppearance.BorderSize = 0;
            this.Lists.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Lists.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lists.ForeColor = System.Drawing.Color.White;
            this.Lists.Image = ((System.Drawing.Image)(resources.GetObject("Lists.Image")));
            this.Lists.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Lists.Location = new System.Drawing.Point(28, 296);
            this.Lists.Name = "Lists";
            this.Lists.Size = new System.Drawing.Size(155, 70);
            this.Lists.TabIndex = 3;
            this.Lists.Text = "       Saved Lists";
            this.Lists.UseVisualStyleBackColor = true;
            this.Lists.Click += new System.EventHandler(this.Lists_Click);
            // 
            // Map
            // 
            this.Map.FlatAppearance.BorderSize = 0;
            this.Map.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Map.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Map.ForeColor = System.Drawing.Color.White;
            this.Map.Image = ((System.Drawing.Image)(resources.GetObject("Map.Image")));
            this.Map.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Map.Location = new System.Drawing.Point(28, 383);
            this.Map.Name = "Map";
            this.Map.Size = new System.Drawing.Size(155, 70);
            this.Map.TabIndex = 3;
            this.Map.Text = "Map";
            this.Map.UseVisualStyleBackColor = true;
            this.Map.Click += new System.EventHandler(this.Map_Click);
            // 
            // Home
            // 
            this.Home.FlatAppearance.BorderSize = 0;
            this.Home.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Home.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Home.ForeColor = System.Drawing.Color.White;
            this.Home.Image = ((System.Drawing.Image)(resources.GetObject("Home.Image")));
            this.Home.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Home.Location = new System.Drawing.Point(28, 209);
            this.Home.Name = "Home";
            this.Home.Size = new System.Drawing.Size(155, 70);
            this.Home.TabIndex = 2;
            this.Home.Text = "Home";
            this.Home.UseVisualStyleBackColor = true;
            this.Home.Click += new System.EventHandler(this.Home_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.BackColor = System.Drawing.Color.Transparent;
            this.ExitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ExitButton.FlatAppearance.BorderSize = 0;
            this.ExitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExitButton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExitButton.ForeColor = System.Drawing.Color.Transparent;
            this.ExitButton.Image = ((System.Drawing.Image)(resources.GetObject("ExitButton.Image")));
            this.ExitButton.Location = new System.Drawing.Point(907, 11);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(44, 51);
            this.ExitButton.TabIndex = 4;
            this.ExitButton.UseVisualStyleBackColor = false;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(181)))), ((int)(((byte)(193)))));
            this.panel2.Controls.Add(this.Settings);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.ExitButton);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(189, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(963, 72);
            this.panel2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Elephant", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(325, 31);
            this.label1.TabIndex = 5;
            this.label1.Text = "Shop Smart. Shop Fast!\r\n";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.Contact);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.Lists);
            this.panel1.Controls.Add(this.SideSelection);
            this.panel1.Controls.Add(this.Home);
            this.panel1.Controls.Add(this.Map);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(189, 750);
            this.panel1.TabIndex = 0;
            // 
            // button4
            // 
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.Location = new System.Drawing.Point(69, 694);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(39, 34);
            this.button4.TabIndex = 11;
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(24, 694);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(39, 34);
            this.button2.TabIndex = 9;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(114, 694);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(39, 34);
            this.button3.TabIndex = 10;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // Contact
            // 
            this.Contact.FlatAppearance.BorderSize = 0;
            this.Contact.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Contact.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Contact.ForeColor = System.Drawing.Color.White;
            this.Contact.Image = ((System.Drawing.Image)(resources.GetObject("Contact.Image")));
            this.Contact.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Contact.Location = new System.Drawing.Point(30, 470);
            this.Contact.Name = "Contact";
            this.Contact.Size = new System.Drawing.Size(155, 70);
            this.Contact.TabIndex = 4;
            this.Contact.Text = "   Contact";
            this.Contact.UseVisualStyleBackColor = true;
            this.Contact.Click += new System.EventHandler(this.button1_Click);
            // 
            // Settings
            // 
            this.Settings.BackColor = System.Drawing.Color.Transparent;
            this.Settings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Settings.FlatAppearance.BorderSize = 0;
            this.Settings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Settings.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Settings.ForeColor = System.Drawing.Color.Transparent;
            this.Settings.Image = ((System.Drawing.Image)(resources.GetObject("Settings.Image")));
            this.Settings.Location = new System.Drawing.Point(846, 11);
            this.Settings.Name = "Settings";
            this.Settings.Size = new System.Drawing.Size(44, 51);
            this.Settings.TabIndex = 6;
            this.Settings.UseVisualStyleBackColor = false;
            // 
            // contactPage1
            // 
            this.contactPage1.BackColor = System.Drawing.Color.Black;
            this.contactPage1.Location = new System.Drawing.Point(189, 69);
            this.contactPage1.Name = "contactPage1";
            this.contactPage1.Size = new System.Drawing.Size(964, 681);
            this.contactPage1.TabIndex = 14;
            // 
            // homePage1
            // 
            this.homePage1.BackColor = System.Drawing.SystemColors.Control;
            this.homePage1.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.homePage1.Location = new System.Drawing.Point(189, 69);
            this.homePage1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.homePage1.Name = "homePage1";
            this.homePage1.Size = new System.Drawing.Size(964, 681);
            this.homePage1.TabIndex = 6;
            // 
            // mapPage1
            // 
            this.mapPage1.Location = new System.Drawing.Point(189, 69);
            this.mapPage1.Name = "mapPage1";
            this.mapPage1.Size = new System.Drawing.Size(963, 681);
            this.mapPage1.TabIndex = 12;
            // 
            // savedLists1
            // 
            this.savedLists1.Location = new System.Drawing.Point(189, 69);
            this.savedLists1.Name = "savedLists1";
            this.savedLists1.Size = new System.Drawing.Size(963, 681);
            this.savedLists1.TabIndex = 13;
            // 
            // MainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1152, 750);
            this.Controls.Add(this.contactPage1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.homePage1);
            this.Controls.Add(this.mapPage1);
            this.Controls.Add(this.savedLists1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainPage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainPage";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button Home;
        private System.Windows.Forms.Panel SideSelection;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.Button Map;
        private System.Windows.Forms.Button Lists;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button Contact;
        private ListCreator homePage1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private MapPage mapPage1;
        private System.Windows.Forms.Label label1;
        private SavedLists savedLists1;
        private System.Windows.Forms.Button Settings;
        private ContactPage contactPage1;
    }
}
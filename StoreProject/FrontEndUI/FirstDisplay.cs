﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEndUI
{
    public partial class FirstDisplay : Form
    {
        public FirstDisplay()
        {
            InitializeComponent();
            this.AcceptButton = StoreButton;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void StoreButton_Click(object sender, EventArgs e)
        {
            if (StoreSelection.Text.ToLower() == "kroger")
            {
                MainPage pageSwitch = new MainPage();
                pageSwitch.Show();
                this.Hide();

            }
            else
            {
                MessageBox.Show("Sorry the store you entered is incorrect or has not been implimented.",
                                "Error",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation,
                                MessageBoxDefaultButton.Button1);
            }
        }

        private void ExitProgram_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}

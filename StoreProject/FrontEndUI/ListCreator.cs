﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibStoreCore;

namespace FrontEndUI
{
    public partial class ListCreator : UserControl
    {
        private List<string> CurrentList = new List<string>();
        Store store = new Store("", "", "", "");
        //Store store = Store.LoadJson("..\\..\\..\\ExampleConfigs\\TestStore.json");


        public ListCreator()
        {
            InitializeComponent();
            //Trying to change up the way I get the info from the database
            //trying to just have a method that sends the loaded store data instead of calling
            //the store at the top. Not sure if im doing it right because ims tarted to get errors
            //When I rn
            //var parent2 = parent as MainPage;
            store = MainPage.getStoreInfo();

            for (int i = 0; i < store.sections.Count(); i++)
            {
                for (int j = 0; j < store.sections[i].products.Count(); j++)
                {
                    ItemSearchBox.AutoCompleteCustomSource.Add(store.sections[i].products[j].name);
                }

            }
        }

        private void ItemSearchBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void AddItemToList_Click(object sender, EventArgs e)
        {
            if (checkIFItemIsInTheDatabase())
            {
                ListBox.Items.Add(ItemSearchBox.Text);
                ItemSearchBox.Text = String.Empty;
            }
            else
            {
                MessageBox.Show("Sorry we don't have that item at this store.",
                                                   "Error",
                                                   MessageBoxButtons.OK,
                                                   MessageBoxIcon.Exclamation,
                                                   MessageBoxDefaultButton.Button1);
            }


        }

        private void ListBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CreateNewList_Click(object sender, EventArgs e)
        {
            CurrentList.Clear();
            if (ListName.Text.Length != 0 && ListBox.Items.Count != 0)
            {
                CurrentList.Add(ListName.Text);
                for (int i = 0; i < ListBox.Items.Count; i++)
                {
                    CurrentList.Add(ListBox.Items[i].ToString());
                }
                //var parent = this.Parent as MainPage;
                //Sends the list to the list storage
                MainPage.addList(CurrentList);
                ListBox.Items.Clear();
                ListName.Text = String.Empty;

            }
            else
            {
                MessageBox.Show("Don't Forget to enter items into the list and a Name for your list!",
                               "Error",
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Exclamation,
                               MessageBoxDefaultButton.Button1);
            }
        }

        private void DeleteItem_Click(object sender, EventArgs e)
        {
            int index = ListBox.SelectedIndex;
            ListBox.Items.RemoveAt(index);
        }

        private Boolean checkIFItemIsInTheDatabase()
        {
            Boolean itemCheck = true;

            for (int i = 0; i < store.sections.Count(); i++)
            {
                for (int j = 0; j < store.sections[i].products.Count(); j++)
                {
                    if (store.sections[i].products[j].name.ToLower() == ItemSearchBox.Text.ToLower())
                    {
                        return itemCheck;
                    }

                }
            }

            return !itemCheck;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibStoreCore;

namespace FrontEndUI
{
    public partial class MapPage : UserControl
    {
        //Store store = Store.LoadJson("..\\..\\..\\ExampleConfigs\\TestStore.json");
        List<List<string>> mainListStorage = new List<List<string>>();
        List<string> selectedList = new List<string>();
        Store store = new Store("", "", "", "");

        public MapPage()
        {
            InitializeComponent();
            //var parent2 = parent as MainPage;
            mainListStorage = MainPage.getListStorage();
            store = MainPage.getStoreInfo();
            this.Enter += Genereate_Click;
            ListNames.SelectedValueChanged += SelectList_Click;
        }

        private void GenerateMap_Click(object sender, EventArgs e)
        {
            var productList = new List<string>();
            for (int i = 0; i < ListItems.Items.Count; i++)
            {
                string item = ListItems.Items[i].ToString();
                productList.Add(item);
            }
            var bestpath = store.FindPathByProducts(productList.ToArray());
            Image final = store.DrawSections(bestpath);
            MapBox.Image = final;
        }

        private void Genereate_Click(object sender, EventArgs e)
        {
            ListNames.Items.Clear();
            List<string> tempList = new List<string>();
            for (int i = 0; i < mainListStorage.Count(); i++)
            {
                tempList = mainListStorage[i];
                ListNames.Items.Add(tempList[0]);
            }

        }

        private void SelectList_Click(object sender, EventArgs e)
        {
            //Populating the list of items within a certain list chosen
            ListItems.Items.Clear();
            List<string> tempList = new List<string>();
            for (int i = 0; i < mainListStorage.Count(); i++)
            {
                tempList = mainListStorage[i];
                if (ListNames.Text.ToLower() == tempList[0].ToLower())
                {
                    selectedList = tempList;
                    break;
                }
            }

            for (int j = 1; j < tempList.Count(); j++)
            {
                ListItems.Items.Add(tempList[j].ToString());
                //MessageBox.Show(tempList[j]);
            }
        }
    }
}

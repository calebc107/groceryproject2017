﻿namespace FrontEndUI
{
    partial class MapPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MapPage));
            this.MapBox = new System.Windows.Forms.PictureBox();
            this.GenerateMap = new System.Windows.Forms.Button();
            this.ListItems = new System.Windows.Forms.ListBox();
            this.ListNames = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.MapBox)).BeginInit();
            this.SuspendLayout();
            // 
            // MapBox
            // 
            this.MapBox.InitialImage = ((System.Drawing.Image)(resources.GetObject("MapBox.InitialImage")));
            this.MapBox.Location = new System.Drawing.Point(252, 132);
            this.MapBox.Name = "MapBox";
            this.MapBox.Size = new System.Drawing.Size(623, 486);
            this.MapBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.MapBox.TabIndex = 0;
            this.MapBox.TabStop = false;
            // 
            // GenerateMap
            // 
            this.GenerateMap.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.GenerateMap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GenerateMap.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GenerateMap.Location = new System.Drawing.Point(57, 541);
            this.GenerateMap.Name = "GenerateMap";
            this.GenerateMap.Size = new System.Drawing.Size(189, 44);
            this.GenerateMap.TabIndex = 1;
            this.GenerateMap.Text = "Generate Map!";
            this.GenerateMap.UseVisualStyleBackColor = false;
            this.GenerateMap.Click += new System.EventHandler(this.GenerateMap_Click);
            // 
            // ListItems
            // 
            this.ListItems.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListItems.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListItems.FormattingEnabled = true;
            this.ListItems.ItemHeight = 19;
            this.ListItems.Location = new System.Drawing.Point(57, 195);
            this.ListItems.Name = "ListItems";
            this.ListItems.Size = new System.Drawing.Size(189, 325);
            this.ListItems.TabIndex = 2;
            // 
            // ListNames
            // 
            this.ListNames.FormattingEnabled = true;
            this.ListNames.Location = new System.Drawing.Point(57, 132);
            this.ListNames.Name = "ListNames";
            this.ListNames.Size = new System.Drawing.Size(189, 21);
            this.ListNames.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(34, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(233, 21);
            this.label1.TabIndex = 4;
            this.label1.Text = "Select one of your saved lists:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(54, 175);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Shopping List:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(927, 74);
            this.panel1.TabIndex = 6;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(845, 74);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(82, 600);
            this.flowLayoutPanel1.TabIndex = 7;
            // 
            // MapPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ListNames);
            this.Controls.Add(this.ListItems);
            this.Controls.Add(this.GenerateMap);
            this.Controls.Add(this.MapBox);
            this.Name = "MapPage";
            this.Size = new System.Drawing.Size(927, 674);
            ((System.ComponentModel.ISupportInitialize)(this.MapBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox MapBox;
        private System.Windows.Forms.Button GenerateMap;
        private System.Windows.Forms.ListBox ListItems;
        private System.Windows.Forms.ComboBox ListNames;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}

﻿using LibStoreCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEndUI
{
    public partial class MainPage : Form
    {
        private static List<List<string>> listStorage = new List<List<string>>();
        static Store store = new Store("", "", "", "");

        public MainPage()
        {
            store = Store.LoadJson("Canton_rd_Kroger.json");
            List<Section> list_of_Sections = new List<Section>();
            InitializeComponent();



            SideSelection.Height = Home.Height;
            homePage1.BringToFront();
        }

        public static List<List<string>> getListStorage()
        {
            return listStorage;
        }

        //Allows the HomePage event handlers to add the current list to the total list ammount
        public static void addList(List<string> currentList)
        {
            //create a copy of the list to add
            var newlist = new List<string>();
            newlist.AddRange(currentList);
            //First element within each list contains the list name
            listStorage.Add(newlist);
        }

        public static void deleteList(int index)
        {
            //First element within each list contains the list name
            listStorage.RemoveAt(index);
        }

        public static Store getStoreInfo()
        {
            return store;
        }

        private void Home_Click(object sender, EventArgs e)
        {
            SideSelection.Height = Home.Height;
            SideSelection.Top = Home.Top;
            homePage1.BringToFront();
        }

        private void Map_Click(object sender, EventArgs e)
        {
            SideSelection.Height = Map.Height;
            SideSelection.Top = Map.Top;
            mapPage1.BringToFront();
        }

        private void Lists_Click(object sender, EventArgs e)
        {
            SideSelection.Height = Lists.Height;
            SideSelection.Top = Lists.Top;
            savedLists1.BringToFront();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SideSelection.Height = Contact.Height;
            SideSelection.Top = Contact.Top;
            contactPage1.BringToFront();
        }

        private void BackButton_Click(object sender, EventArgs e)
        {

        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            //listStorage.Clear();
            Application.Exit();
        }


    }
}

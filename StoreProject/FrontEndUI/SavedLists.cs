﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibStoreCore;

namespace FrontEndUI
{
    public partial class SavedLists : UserControl
    {
        //Store store = Store.LoadJson("..\\..\\..\\ExampleConfigs\\TestStore.json");
        List<List<string>> mainListStorage = new List<List<string>>();
        List<string> currentList = new List<string>();
        Store store = new Store("", "", "", "");

        public SavedLists()
        {
            InitializeComponent();
            this.Enter += Populate_Click;
            SavedListNames.SelectionChangeCommitted += SelectList_Click;
            //Trying to populate the drop down list of saved lists
            mainListStorage = MainPage.getListStorage();
            store = MainPage.getStoreInfo();

            for (int i = 0; i < store.sections.Count(); i++)
            {
                for (int j = 0; j < store.sections[i].products.Count(); j++)
                {
                    ItemSearchBox.AutoCompleteCustomSource.Add(store.sections[i].products[j].name);
                }

            }
        }

        private void SelectList_Click(object sender, EventArgs e)
        {
            //Populating the list of items within a certain list chosen
            ListItemsCheckBox.Items.Clear();
            List<string> tempList = new List<string>();
            for (int i = 0; i < mainListStorage.Count(); i++)
            {
                tempList = mainListStorage[i];
                if (SavedListNames.Text.ToLower() == tempList[0].ToLower())
                {
                    break;
                }
            }

            currentList = tempList;

            for (int j = 1; j < tempList.Count(); j++)
            {
                ListItemsCheckBox.Items.Add(tempList[j].ToString());
                //MessageBox.Show(tempList[j]);
            }
        }

        private void UpdateList_Click(object sender, EventArgs e)
        {
            //deletes certain items within the list
            foreach (int indexChecked in ListItemsCheckBox.CheckedIndices)
            {
                // The indexChecked variable contains the index of the item.
                for(int i = 1; i < currentList.Count(); i++)
                {
                    if(currentList[i].ToLower() == ListItemsCheckBox.Items[indexChecked].ToString().ToLower())
                    {
                        currentList.RemoveAt(i);
                        ListItemsCheckBox.Items.RemoveAt(indexChecked);
                    }
                }
                
            }


        }

        private void DeleteList_Click(object sender, EventArgs e)
        {
            //deletes the chosen list
            List<string> tempList = new List<string>();
            for (int i = 0; i < mainListStorage.Count(); i++)
            {
                tempList = mainListStorage[i];
                if(currentList[0].ToLower() == tempList[0].ToLower())
                {
                    MainPage.deleteList(i);
                    break;
                }
            }
            //Clears the items related to the list
            SavedListNames.Items.Remove(SavedListNames.Text);
            SavedListNames.Text = String.Empty;
            ListItemsCheckBox.Items.Clear();
        }

        private void AddItem_Click(object sender, EventArgs e)
        {
            //adds items to a selected list
            if (checkIFItemIsInTheDatabase())
            {
                ListItemsCheckBox.Items.Add(ItemSearchBox.Text);
                currentList.Add(ItemSearchBox.Text);
                ItemSearchBox.Text = String.Empty;
            }
            else
            {
                MessageBox.Show("Sorry we don't have that item at this store.",
                                                   "Error",
                                                   MessageBoxButtons.OK,
                                                   MessageBoxIcon.Exclamation,
                                                   MessageBoxDefaultButton.Button1);
            }
        }

        private void Populate_Click(object sender, EventArgs e)
        {
            SavedListNames.Items.Clear();
            List<string> tempList = new List<string>();
            for (int i = 0; i < mainListStorage.Count(); i++)
            {
                tempList = mainListStorage[i];
                SavedListNames.Items.Add(tempList[0]);
            }

        }

        private Boolean checkIFItemIsInTheDatabase()
        {
            Boolean itemCheck = true;

            for (int i = 0; i < store.sections.Count(); i++)
            {
                for (int j = 0; j < store.sections[i].products.Count(); j++)
                {
                    if (store.sections[i].products[j].name.ToLower() == ItemSearchBox.Text.ToLower())
                    {
                        return itemCheck;
                    }

                }
            }

            return !itemCheck;
        }

    }
}

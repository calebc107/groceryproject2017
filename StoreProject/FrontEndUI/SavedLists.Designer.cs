﻿namespace FrontEndUI
{
    partial class SavedLists
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SavedLists));
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.SavedListNames = new System.Windows.Forms.ComboBox();
            this.SelectList = new System.Windows.Forms.Button();
            this.UpdateList = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Populate = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ListItemsCheckBox = new System.Windows.Forms.CheckedListBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.DeleteList = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.ItemSearchBox = new System.Windows.Forms.TextBox();
            this.AddItem = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // SavedListNames
            // 
            this.SavedListNames.FormattingEnabled = true;
            this.SavedListNames.Location = new System.Drawing.Point(132, 348);
            this.SavedListNames.Name = "SavedListNames";
            this.SavedListNames.Size = new System.Drawing.Size(179, 21);
            this.SavedListNames.TabIndex = 0;
            // 
            // SelectList
            // 
            this.SelectList.FlatAppearance.BorderSize = 0;
            this.SelectList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SelectList.Location = new System.Drawing.Point(109, 15);
            this.SelectList.Name = "SelectList";
            this.SelectList.Size = new System.Drawing.Size(75, 23);
            this.SelectList.TabIndex = 1;
            // 
            // UpdateList
            // 
            this.UpdateList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UpdateList.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UpdateList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(181)))), ((int)(((byte)(193)))));
            this.UpdateList.Location = new System.Drawing.Point(32, 205);
            this.UpdateList.Name = "UpdateList";
            this.UpdateList.Size = new System.Drawing.Size(176, 40);
            this.UpdateList.TabIndex = 2;
            this.UpdateList.Text = "Update List";
            this.UpdateList.UseVisualStyleBackColor = true;
            this.UpdateList.Click += new System.EventHandler(this.UpdateList_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(45, 206);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(348, 92);
            this.label1.TabIndex = 3;
            this.label1.Text = "Welcome to your saved grocery lists!\r\nBy going through the drop down \r\nbelow you " +
    "can choose a specific list \r\nyou would like to delete or change!";
            // 
            // label2
            // 
            this.label2.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(2, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(262, 138);
            this.label2.TabIndex = 4;
            this.label2.Text = "You can edit your selected \r\nlist by checking the items \r\nyou don\'t want in the l" +
    "ist \r\nand updating or deleting \r\nyour list completley with\r\nthe buttons below.\r\n" +
    "";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.SavedListNames);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.ListItemsCheckBox);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(963, 681);
            this.panel1.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(130, 322);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(181, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Please select one of your saved lists:";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel2.Controls.Add(this.SelectList);
            this.panel2.Controls.Add(this.Populate);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Location = new System.Drawing.Point(-25, 16);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(970, 163);
            this.panel2.TabIndex = 13;
            // 
            // Populate
            // 
            this.Populate.FlatAppearance.BorderSize = 0;
            this.Populate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Populate.Location = new System.Drawing.Point(28, 14);
            this.Populate.Name = "Populate";
            this.Populate.Size = new System.Drawing.Size(75, 23);
            this.Populate.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Elephant", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(181)))), ((int)(((byte)(193)))));
            this.label3.Location = new System.Drawing.Point(52, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(422, 83);
            this.label3.TabIndex = 14;
            this.label3.Text = "Saved Lists!";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(568, 14);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(253, 126);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 12;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(59, 392);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(334, 247);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // ListItemsCheckBox
            // 
            this.ListItemsCheckBox.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListItemsCheckBox.FormattingEnabled = true;
            this.ListItemsCheckBox.Location = new System.Drawing.Point(443, 219);
            this.ListItemsCheckBox.Name = "ListItemsCheckBox";
            this.ListItemsCheckBox.Size = new System.Drawing.Size(218, 420);
            this.ListItemsCheckBox.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.UpdateList);
            this.panel3.Controls.Add(this.DeleteList);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.ItemSearchBox);
            this.panel3.Controls.Add(this.AddItem);
            this.panel3.Location = new System.Drawing.Point(681, 162);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(264, 521);
            this.panel3.TabIndex = 15;
            // 
            // DeleteList
            // 
            this.DeleteList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeleteList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(181)))), ((int)(((byte)(193)))));
            this.DeleteList.Location = new System.Drawing.Point(32, 262);
            this.DeleteList.Name = "DeleteList";
            this.DeleteList.Size = new System.Drawing.Size(176, 43);
            this.DeleteList.TabIndex = 8;
            this.DeleteList.Text = "Delete List";
            this.DeleteList.UseVisualStyleBackColor = true;
            this.DeleteList.Click += new System.EventHandler(this.DeleteList_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(6, 329);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(258, 46);
            this.label4.TabIndex = 11;
            this.label4.Text = "Use the textbox below to\r\nadd more items to your list!\r\n";
            // 
            // ItemSearchBox
            // 
            this.ItemSearchBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ItemSearchBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ItemSearchBox.Location = new System.Drawing.Point(32, 457);
            this.ItemSearchBox.Name = "ItemSearchBox";
            this.ItemSearchBox.Size = new System.Drawing.Size(176, 20);
            this.ItemSearchBox.TabIndex = 9;
            // 
            // AddItem
            // 
            this.AddItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddItem.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(181)))), ((int)(((byte)(193)))));
            this.AddItem.Location = new System.Drawing.Point(32, 409);
            this.AddItem.Name = "AddItem";
            this.AddItem.Size = new System.Drawing.Size(176, 33);
            this.AddItem.TabIndex = 7;
            this.AddItem.Text = "Add Item to List";
            this.AddItem.UseVisualStyleBackColor = true;
            this.AddItem.Click += new System.EventHandler(this.AddItem_Click);
            // 
            // SavedLists
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "SavedLists";
            this.Size = new System.Drawing.Size(963, 681);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ComboBox SavedListNames;
        private System.Windows.Forms.Button SelectList;
        private System.Windows.Forms.Button UpdateList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckedListBox ListItemsCheckBox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ItemSearchBox;
        private System.Windows.Forms.Button DeleteList;
        private System.Windows.Forms.Button AddItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button Populate;
        private System.Windows.Forms.Label label5;
    }
}
